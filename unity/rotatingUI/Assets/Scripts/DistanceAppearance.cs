﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

// convenience class for objects that have an attached WebVideoPlayer
public class DistanceAppearance : MonoBehaviour
{
    public float distanceToBeActive = 10;
    bool isActive = false;

    // store local position, to calculate distance to player 
    Vector3 localPosition;
    public GameObject disappear;

    GameObject player;

    public void Start() {
        localPosition = gameObject.transform.position;

        player = GameObject.Find("FirstPerson-AIO");

        disappear.SetActive(false);
    }

    public void Update()
    {
        float dist = Vector3.Distance(localPosition, player.transform.position);
        bool shouldBeActive = dist < distanceToBeActive;

        if (isActive && !shouldBeActive) {
            disappear.SetActive(false);
            isActive = false;
        } else if (!isActive && shouldBeActive) {
            disappear.SetActive(true);
            isActive = true;
        }
    }
}
