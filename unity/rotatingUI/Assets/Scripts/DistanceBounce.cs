﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

// convenience class for objects that have an attached WebVideoPlayer
public class DistanceBounce : MonoBehaviour
{
    public float distanceToBeActive = 10;
    bool isActive = false;

    // store local position, to calculate distance to player 
    Vector3 localPosition;
    public GameObject target;
    public float speed = 1.0f;

    GameObject player;

    public void Start() {
        localPosition = gameObject.transform.position;

        player = GameObject.Find("FirstPerson-AIO");
    }

    public void Update()
    {
        float dist = Vector3.Distance(localPosition, player.transform.position);
        bool shouldBeActive = dist < distanceToBeActive;

        if (isActive && !shouldBeActive) {
            isActive = false;
        } else if (!isActive && shouldBeActive) {
            isActive = true;
        }

        float time = Time.deltaTime;

        if (isActive) {
            target.transform.Translate(Mathf.Sin(time),0,0);
        }
        Debug.Log(time);
    }
}
