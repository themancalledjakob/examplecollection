using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

// convenience class for objects that have an attached WebVideoPlayer
public class DistanceRotation : MonoBehaviour
{
    public float distanceToBeActive = 10;
    bool isActive = false;

    // store local position, to calculate distance to player 
    Vector3 localPosition;
    public Vector3 rotations;
    public GameObject rotatingObject;

    GameObject player;

    public void Start() {

        player = GameObject.Find("FirstPerson-AIO");
    }

    public void Update()
    {
        float dist = Vector3.Distance(transform.position, player.transform.position);
        bool shouldBeActive = dist < distanceToBeActive;

        if (isActive && !shouldBeActive) {
            isActive = false;
        } else if (!isActive && shouldBeActive) {
            isActive = true;
        }

        if (isActive) {
            rotatingObject.transform.Rotate(rotations);
        }
    }
}
