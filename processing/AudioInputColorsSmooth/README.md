 Grab audio from the microphone input and draw a circle whose size
 is determined by how loud the audio input is.
 
in this example, we also change colors smoothly with the microphone

 # Dependencies (a.k.a. libraries you need to install):
 - Sound
 
 # Install dependency:
 - click in menu bar on: Sketch/Import Library/Add Library
 - search for dependency (for example "Sound")
 - click install
 - restart processing to load examples in example browser (optional)
 
 # Notes:
 this example loads the first system microphone, so if you want to use
 an external microphone, just set it as default in your system settings
 and processing will load that

# Preview:
![Sample Video](preview.mp4)
