/**
 * how to use sound to draw images
 *
 * we analyze the sound, to get the loudness of frequencies from low to high
 * kind of like an equalizer of an old hifi system
 * 
 * then we connect images to the frequencies and control their transparency
 * with the loudness of a particular frequency
 *
 * if you change the images in the data directory then you can draw with sound!
 * 
 * Dependencies (a.k.a. libraries you need to install):
 * - Sound
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for the name of the dependency (e.g. "Sound", or "OpenCV for Processing")
 * - click install
 * - restart processing to load examples in example browser (optional)
 **/

import processing.sound.*;

AudioIn input;
FFT fft;

// Define how many FFT bands to use (this needs to be a power of two)
// valid is: 2, 4, 8, 16, 32, 64, 128, 256, etc
int bands = 128;

// define a list of images
ArrayList<PImage> images= new ArrayList<PImage>();

void setup() {
  // let's do it fullscreen
  fullScreen(P2D);

  // Create an Audio input and grab the 1st channel
  input = new AudioIn(this, 0);

  // Begin capturing the audio input
  input.start();

  // create the FFT object to analyze sound later, and cut it into frequency-bands
  // the amount we defined above with "int bands = blabla;"
  fft = new FFT(this, bands);

  // let's tell the fft object to use the microphone input as input for analysis
  fft.input(input);

  // define the amount of images to load
  int amountImages = 16;

  // use a for-loop to go over all images
  // if you want to know more about what a for-loop is,
  // check this: https://www.youtube.com/watch?v=h4ApLHe8tbk
  for (int i = 0; i < amountImages; i++) {
    // load the images into the list
    // watch out, your own images have to be named:
    // image0.png
    // image1.png
    // image2.png
    // and so on, there should not be a gap!
    // if you have for example 8 images, your last image should be called "image7.png"
    images.add(loadImage("image" + i + ".png"));
  }
  // draw a black background on startup
  background(0);
}

void draw() {
  // perform a forward FFT on the samples in input buffer
  fft.analyze();

  //then, go through all frequency bands
  for (int i = 0; i < bands; i++) {
    // first, we need the amplitude (or loudness) of the current frequency
    float amplitude = fft.spectrum[i];

    // we can use map to scale amplitude up to what we need as an opacity
    // amplitude is always between 0 and 1, but we want 0 to 255
    // we can map 0.05 to 0 and 1 to 255
    float opacity = map(amplitude, 
      0.05, // microphone minimum (a bit higher than zero to filter out background noise)
      1, // input maximum
      0, // opacity minimum
      255); // opacity maximum

    // now use the opacity as a "tint" filter
    // check this https://processing.org/reference/tint_.html
    tint(255, int(opacity));
    // and draw the image!
    image(images.get(i%images.size()), 0, 0, width, height);
  }
}
