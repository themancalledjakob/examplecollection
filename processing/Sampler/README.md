This example shows how to make a simple sampler and sequencer with the Sound library.
Five different samples are loaded and played back at different pitches, in this
case 5 different octaves. The sequencer randomly triggers an event every
200-1000 mSecs. Each time a sound is played a colored rect with a random color is
displayed.

This is an original example from the sound library, it's just here because we'll build up on it

# Dependencies (a.k.a. libraries you need to install):
 - Sound

# Install dependency:
- click in menu bar on: Sketch/Import Library/Add Library
- search for dependency (for example "Sound", don't search for "dependency")
- click install
- restart processing to load examples in example browser (optional)

# Preview:
![Sample Video](preview.mp4)
