/**
 * Pointillism
 * by Daniel Shiffman. 
 * 
 * Mouse horizontal location controls size of dots. 
 * Creates a simple pointillist effect using ellipses colored
 * according to pixels in an image. 
 */

PImage img;

void setup() {
  // set the size of the sketch
  size(640, 360);
  
  // load the image (it has to be in the data folder of the sketch)
  // you can open this by clicking on "Sketch/Show Sketch Folder" and look in the "data" folder in there
  // or just drag and drop an image inside the code ---> here <--- , or anywhere else in the code
  img = loadImage("moonwalk.jpg");
  
  // if you use noStroke, then all shapes drawn after this dont have strokes
  // leave it away to see what happens
  noStroke();
}

void draw() {
  // draw a black background
  background(0);
  
  // now this is where it gets a bit tricky,
  // basically, we decide here how much spacing we want inbetween the pointilism shapes
  // and link it to the mouse position.
  // if mouse is on the left (0), then we want the shapes to be "4" pixels big
  // if the mouse is on the right (width), then we want the shapes to be "32" pixels big
  // rightclick on "map" and "find in reference" to know more about it
  int spacing = (int)map(mouseX,0,width,4,32);
  
  // these are for-loops (https://processing.org/reference/for.html)
  // basically an image has two dimensions. the x and the y-dimension.
  // in other words, up/down and left/right. (https://processing.org/tutorials/pixels/)
  // with two for loops we can read the pixels of the image, and use their brightness
  // to change the shapes
  for (int x = 0; x < img.width; x = x + spacing) {
    for (int y = 0; y < img.height; y = y + spacing) {
      
      // we can get a pixel color at a specific coordinate on the image
      // like this:
      color pixel = img.get(x, y);
      
      // the following is some magic to get red, green and blue values from the pixel
      int r = (pixel >> 16) & 0xFF;  // Faster way of getting red(pixel)
      int g = (pixel >> 8) & 0xFF;   // Faster way of getting green(pixel)
      int b = pixel & 0xFF;          // Faster way of getting blue(pixel)
      
      // add red, green and blue together to get the total brightness
      // remember, red, green and blue go from 0 to 255
      // so brightness can be from 0 to 765
      float brightness = r + g + b;
      
      // the size of the ellipses is always the same
      // can you make it also dependent on the image?
      float size = 5;
      
      // set the fill-color of the ellipse
      fill(brightness/3);
      
      // draw a single ellipse!
      // remember that this sits in a for-loop, so we actually do this a lot of times,
      // so you will see a lot of ellipses
      ellipse(x,y, size, size);
    }
  }
}
