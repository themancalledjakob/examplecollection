/**
 * Grab audio from the microphone input and overlay a images
 * depending how loud the audio input is
 * 
 * Dependencies (a.k.a. libraries you need to install):
 * - Sound
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 * Notes:
 * this example loads the first system microphone, so if you want to use
 * an external microphone, just set it as default in your system settings
 * and processing will load that
 */

import processing.sound.*;

// create global input and loudness objects
// these we need to create above "setup()" and "draw()",
// because we want to access them in both
AudioIn input;
Amplitude loudness;

// if we want to smooth the audio,
// we need to also create a global variable for the volume
float volume = 0;

// create the objects for images, also global!
PImage tiger1;
PImage tiger2;

void setup() {
  // the setup we just leave as it was
  // the colors are set when we draw, so we only
  // change things in the draw-function below
  size(640, 360);
  background(255);

  // Create an Audio input and grab the 1st channel
  input = new AudioIn(this, 0);

  // Begin capturing the audio input
  input.start();
  // start() activates audio capture so that you can use it as
  // the input to live sound analysis, but it does NOT cause the
  // captured audio to be played back to you. if you also want the
  // microphone input to be played back to you, call
  //    input.play();
  // instead (be careful with your speaker volume, you might produce
  // painful audio feedback. best to first try it out wearing headphones!)

  // Create a new Amplitude analyzer
  loudness = new Amplitude(this);

  // Patch the input to the volume analyzer
  loudness.input(input);
  
  // load the tiger images
  tiger1 = loadImage("tiger1.jpg");
  tiger2 = loadImage("tiger2.jpg");
}

// this following is the draw function
void draw() {
  // Adjust the volume of the audio input based on mouse position
  // hmmm.... so this adjusts the sensitivity of the microphone
  // right now, we don't care about sensitivity
  // we just want the colors to change, the sensitivity we can leave at the default.
  // so we comment the next two lines out
  // float inputLevel = map(mouseY, 0, height, 1.0, 0.0);
  // input.amp(inputLevel);

  // loudness.analyze() return a value between 0 and 1.
  // just a bit clarification: volume is a variable
  // which we set to the loudness of the microphone
  // therefore it makes sense to call it "volume"
  // if we want to smooth it, we need to calculate the old value into the new value
  
  // so let's save the old volume in a variable
  float oldVolume = volume;
  // put the currently analyzed volume in a variable as well
  float newVolume = loudness.analyze();
  
  // this is the amount of smoothing
  // 0 = no smoothing
  // 0.25 = almost not smoothed
  // 0.5 = half smoothed
  // 0.8 = very smooth
  // 0.99 = very very smooth
  // 1 = no change
  float smoothing = 0.8;

  // define how old and new should be mixed together
  // don't change these, but change "smoothing" above instead
  // so that oldMix and newMix are together always 1.0
  float oldMix = smoothing;
  float newMix = 1.0-smoothing;
  
  // mix together oldVolume and newVolume
  // the more old volume there is in the mix, the smoother
  volume = oldVolume * oldMix + newVolume * newMix;
  
  // and now, draw the image of our first tiger with full opacity
  tint(255,255,255,255);
  image(tiger1,0,0,width,height);
  
  // we can use "tint" to change opacity of our images
  // map the opacity in a similar way as above, but this time
  // we write the minimum/maximum values directly in the map function
  // notice how small the minimum and maximum values are
  // you might need to adjust them to your microphone a bit
  // the first one needs to be smaller than the second
  int opacity1 = int(map(volume, 0.05, 0.1, 0, 255));
  // if you want to know more about the "map" function, just right-click on map
  // and select "Find in Reference" and it will open the documentation for the "map" function
  
  tint(255,255,255,opacity1);
  image(tiger2,0,0,width,height);
}
