/**
 * Grab video and use optical flow for drawing an image
 * 
 * Dependencies (a.k.a. libraries you need to install):
 * - Video
 * - OpenCV for Processing
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 */
import gab.opencv.*;
import processing.video.*;

OpenCV opencv;
Capture video;

PImage tiger_left;
PImage tiger_right;
PImage tiger_up;
PImage tiger_down;

// let's draw the camera, so we see if it works properly
// see below how you can toggle it on keypress
boolean drawCamera = true;

void setup() {
  size(1280, 720, P3D);
  video = new Capture(this, 320, 240);
  opencv = new OpenCV(this, 320, 240);
  video.start();
  background(0);
  
  tiger_left = loadImage("tiger_left.jpg");
  tiger_right = loadImage("tiger_right.jpg");
  tiger_up = loadImage("tiger_up.jpg");
  tiger_down = loadImage("tiger_down.jpg");
}

void draw() {

  opencv.loadImage(video);
  //opencv.threshold(250);
  opencv.calculateOpticalFlow();
  
  PVector flowVec = opencv.getAverageFlow();
  float intensity = 50;
  int opacityTiger1 = int(max(0,flowVec.x * -intensity));
  int opacityTiger2 = int(max(0,flowVec.x * intensity));
  int opacityTiger3 = int(max(0,flowVec.y * -intensity));
  int opacityTiger4 = int(max(0,flowVec.y * intensity));
  
  tint(255,255,255,opacityTiger1);
  image(tiger_left,0,0,width,height);
  tint(255,255,255,opacityTiger2);
  image(tiger_right,0,0,width,height);
  tint(255,255,255,opacityTiger3);
  image(tiger_up,0,0,width,height);
  tint(255,255,255,opacityTiger4);
  image(tiger_down,0,0,width,height);
  
  if (drawCamera) {
    tint(255,255,255,255);
    image(video,0,0,160,90);
  }
}

void keyReleased() {
  // erase the drawing with space
  if (key == ' ') {
    background(0);
  }
  
  if (key == 's') {
    saveFrame("screenshot-######.png");
  }
  
  if (key == 'c') {
    // you can toggle a boolean between true and false if you
    // set it to the opposite of itself. so, if it is true,
    // then it is set to false and the other way around.
    // like this you can endlessly toggle it
    drawCamera = !drawCamera;
  }
}

void captureEvent(Capture c) {
  c.read();
}
