/**
 * This sketch shows how to use the FFT class to analyze a stream
 * of sound. Change the number of bands to get more spectral bands
 * (at the expense of more coarse-grained time resolution of the spectrum).
 *
 * It is based on example Sound/Analysis/FFTSpectrum
 *
 * Dependencies (a.k.a. libraries you need to install):
 * - Sound
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 */

import processing.sound.*;

// Declare the sound source and FFT analyzer variables
SoundFile sample;
FFT fft;

// Define how many FFT bands to use (this needs to be a power of two)
int bands = 128;

// Define a smoothing factor which determines how much the spectrums of consecutive
// points in time should be combined to create a smoother visualisation of the spectrum.
// A smoothing factor of 1.0 means no smoothing (only the data from the newest analysis
// is rendered), decrease the factor down towards 0.0 to have the visualisation update
// more slowly, which is easier on the eye.
float smoothingFactor = 0.2;

// Create a vector to store the smoothed spectrum data in
float[] sum = new float[bands];

// Variables for drawing the spectrum:
// Declare a scaling factor for adjusting the height of the rectangles
int scale = 5;
// Declare a drawing variable for calculating the height of the 
float barHeight;

// we creep always one line to the right
int creep = 0;

public void setup() {
  size(1280, 256);
  background(255);

  // Calculate the height of the rects depending on how many bands we have
  barHeight = height/float(bands);

  // Load and play a soundfile and loop it.
  sample = new SoundFile(this, "tiger.mp3");
  sample.loop();

  // Create the FFT analyzer and connect the playing soundfile to it.
  fft = new FFT(this, bands);
  fft.input(sample);
  background(0, 0, 0);
  
  // we can control the speed easily by adjusting the frames per second that our application runs
  int speed = 120;
  frameRate(speed);
}

public void draw() {
  // Set background color, noStroke and fill color
  fill(255, 0, 150);
  noStroke();

  // Perform the analysis
  fft.analyze();
  
  for (int i = 0; i < bands; i++) {
    // Smooth the FFT spectrum data by smoothing factor
    sum[i] += (fft.spectrum[i] - sum[i]) * smoothingFactor;

    // Draw the rectangles, adjust their red, green and blue values (r,g,b)
    // if you play with the values, you can change the color
    float r = map(sum[i],0,0.05,0,255);
    float g = map(sum[i],0,0.2,60,255);
    float b = 125;
    fill(r,g,b);
    rect(creep* barHeight, i*barHeight, barHeight, barHeight);
  }
  
  // now, that we drew a line, let's increment creep, to jump to the next line next time
  creep = creep + 1;
  
  // jump back to the first line, if we get out of range
  if (creep > width/barHeight) {
    creep = 0;
  }
}
