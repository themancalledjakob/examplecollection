 This sketch shows how to use the FFT class to analyze a stream
 of sound. Change the number of bands to get more spectral bands
 (at the expense of more coarse-grained time resolution of the spectrum).
 
It is based on example Sound/Analysis/FFTSpectrum

 # Dependencies (a.k.a. libraries you need to install):
 - Sound
 
 # Install dependency:
 - click in menu bar on: Sketch/Import Library/Add Library
 - search for dependency (for example "Sound")
 - click install
 - restart processing to load examples in example browser (optional)
 
 # Notes:
 this example loads the first system microphone, so if you want to use
 an external microphone, just set it as default in your system settings
 and processing will load that

# Preview:
![Sample Video](preview.mp4)
