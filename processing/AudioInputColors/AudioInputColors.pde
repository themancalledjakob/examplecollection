/**
 * Grab audio from the microphone input and draw a circle whose size
 * is determined by how loud the audio input is.
 * 
 * in this example, we also change colors with the microphone
 *
 * Dependencies (a.k.a. libraries you need to install):
 * - Sound
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 * Notes:
 * this example loads the first system microphone, so if you want to use
 * an external microphone, just set it as default in your system settings
 * and processing will load that
 */

import processing.sound.*;

// create global input and loudness objects
// these we need to create above "setup()" and "draw()",
// because we want to access them in both
AudioIn input;
Amplitude loudness;

void setup() {
  // the setup we just leave as it was
  // the colors are set when we draw, so we only
  // change things in the draw-function below
  size(640, 360);
  background(255);

  // Create an Audio input and grab the 1st channel
  input = new AudioIn(this, 0);

  // Begin capturing the audio input
  input.start();
  // start() activates audio capture so that you can use it as
  // the input to live sound analysis, but it does NOT cause the
  // captured audio to be played back to you. if you also want the
  // microphone input to be played back to you, call
  //    input.play();
  // instead (be careful with your speaker volume, you might produce
  // painful audio feedback. best to first try it out wearing headphones!)

  // Create a new Amplitude analyzer
  loudness = new Amplitude(this);

  // Patch the input to the volume analyzer
  loudness.input(input);
}

// this following is the draw function
void draw() {
  // Adjust the volume of the audio input based on mouse position
  // hmmm.... so this adjusts the sensitivity of the microphone
  // right now, we don't care about sensitivity
  // we just want the colors to change, the sensitivity we can leave at the default.
  // so we comment the next two lines out
  // float inputLevel = map(mouseY, 0, height, 1.0, 0.0);
  // input.amp(inputLevel);

  // loudness.analyze() return a value between 0 and 1. To adjust
  // the scaling and mapping of an ellipse we scale from 0 to 0.5
  // 
  // just a bit clarification: volume is a variable
  // which we set to the loudness of the microphone
  // therefore it makes sense to call it "volume"
  float volume = loudness.analyze();
  
  // here, we set a size and use a fancy function called "map",
  // because our volume is always between 0 and 1.
  // When we draw the circle, we want to give it a size much bigger though,
  // so when our volume is 0, we want the circle to be 20 pixels big,
  // and when our volume is 1 we want it to be 700.
  //
  float minimumVolume = 0;
  float maximumVolume = 1;
  float minimumSize = 20; // when volume is equal to minimumVolume, we want our circle to be 20 px big.
  float maximumSize = 700; // when volume is equal to maximumVolume, we want our size to be 700 px big.
  // 
  // in the example before, the variable "size" was of the type "int" (Integer)
  // an "int" is also a number, like "float", but it is always a rounded number
  // so, it can only be 0, 1, 2, 3 .. and so on, but not 0.5 or 0.123 like a float could be.
  // 
  // anyways, let's throw in the variables from before.
  int size = int(map(volume, minimumVolume, maximumVolume, minimumSize, maximumSize));
  // if you want to know more about the "map" function, just right-click on map
  // and select "Find in Reference" and it will open the documentation for the "map" function

  // now, let's get to the good part,
  // and change the color with the volume
  // we can mix a color with setting the values
  // for the primary colors red, green and blue.
  // the values are background(red, green, blue)
  // they range from 0 to 255.
  // 0 means, don't mix in the color
  // and 255 means, mix it in full
  // background(255,0,0) would be completely red
  // backgorund(0,255,0) would be completely green
  // and so on
  
  background(125, 255 * volume, 125 + 125 * volume);
  // btw, can you make it change from white to black?
  
  // if you don't say "noStroke()", your circle will have a stroke around it.
  // let's see how that looks like
  //noStroke();
  // instead, we can give the stroke a color that reacts on the microphone
  stroke(255*volume,255*volume,255*volume);
  
  // map the red color of the circle
  // in a similar way as above, but this time
  // we write the minimum/maximum values directly in the map function
  int red = int(map(volume, 0, 0.1, 0, 255));
  fill(red, 0, 150);
  // We draw a circle whose size is coupled to the audio analysis
  ellipse(width/2, height/2, size, size);
}
