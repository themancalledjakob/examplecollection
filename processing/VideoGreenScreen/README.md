how to  display a greenscreen video
where the green acts as a mask and is transparent
 

 # Dependencies (a.k.a. libraries you need to install):
 - Video
 
 # Install dependency:
 - click in menu bar on: Sketch/Import Library/Add Library
 - search for dependency (for example "Sound")
 - click install
 - restart processing to load examples in example browser (optional)
 
 # Notes:
this example uses an advanced, modern way of doing a greenscreen with your graphic card.
you should be able to apply this to your sketches in the same way as shown here.
if anything is unclear, please ask.

# Preview:
![Sample Video](preview.mp4)
