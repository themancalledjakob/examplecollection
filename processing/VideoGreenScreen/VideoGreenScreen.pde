/**
 * Video Green Screen
 * 
 * how to  display a greenscreen video
 * where the green acts as a mask and is transparent
 * 
 * Dependencies (a.k.a. libraries you need to install):
 * - Video
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 */

import processing.video.*;

// this example uses a modern approach to calculate the greenscreen
// directly on your graphic card
// this means it's superfast, and we need to use something called a "shader"
// don't worry about it too much, you don't need to change that.
// but just so you know, a shader is a textfile which acts like a filter for what you draw
// Disclaimer: this is technically not entirely correct, but you can imagine it like that here.
// if you want to know more, check this: https://en.wikipedia.org/wiki/Shader
// 
// anyway, here the shader object, just keep it there
PShader maskShader;

// we want to draw a greenscreen movie, so let's create the object
Movie movie;

// why not draw a background image to check that our masking works
PImage backgroundImage;

void setup() {
  // always, when using graphically advanced techniques (like here, a shader)
  // you should use an advanced renderer. what that exactly means, you can read
  // up on here: https://processing.org/tutorials/p3d/
  // but you don't really need to know that.
  // just set the window size like this and you're good:
  size(640, 360, P3D);

  // let's load our background image
  backgroundImage = loadImage("leaves.jpg");

  // and then load the movie
  movie = new Movie(this, "tiger_greenscreen.mp4");

  // start the movie, and loop it at the same time
  movie.loop();

  // this is loading the textfile of the shader
  // it is also in the data directory.
  // if you want to use this somewhere else, you also need to copy the textfile
  maskShader = loadShader("mask.glsl");

  // set the "transparent" color in red, green and blue
  // minimum is 0
  // and maximum is 1
  // our example video has a "transparent" color of completely green,
  // so we do this
  float red = 0;
  float green = 1;
  float blue = 0;

  // this sets the color for our shader
  maskShader.set("keyColor", red, green, blue); 

  // these are some parameters for the green screen masking filter effect
  // they go from 0 to 1
  // default values are:
  // similarity = 0.4
  // smoothness = 0.08
  // spill = 0.1
  //
  // if it doesn't look nice, adjust these
  maskShader.set("similarity", 0.4);
  maskShader.set("smoothness", 0.08);
  maskShader.set("spill", 0.1);

  // draw a red background first, why not
  background(255, 0, 0);
}

// wheew, the draw function!
void draw() {
  // let's draw the background image
  // we could do
  // image(backgroundImage, 0, 0, width, height);
  // but there is a shortcut:
  background(backgroundImage);
  
  // everything drawn after this is going to be green screened
  shader(maskShader);    
  image(movie, 0, 0, width, height);
  
  // if you want to draw something later without green screening,
  // you can deactivate the shader
  // then anything you draw is drawn normally
  resetShader();
  
  // now draw a little thumbnail of our video
  // firsts, set width and height
  int w = 120;
  int h = 60;
  
  // then draw it in the center of our cursor
  // 
  // the center is always the position of the cursor
  // and shift it to left, with half of the width of the thumbnail
  // and shift it up, width half of the height of the thumbnail
  // so we can substract these from the mouse postition like that:
  image(movie, // our image (the movie)
    mouseX-(w/2), // mouse position minus half the width
    mouseY-(h/2), // mouse position minus half the height
    w, h); // and then just width and height of the thumbnail
}

// so. a movie is basically a collection of images
// we first show image number one, then two, then three and so on
// here we update the movie 
void movieEvent(Movie m) {
  m.read();
}
