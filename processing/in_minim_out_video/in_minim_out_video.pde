/**
 * in_sound_out_video
 * 
 * Use sound to control video playback
 * 
 */
 
// first, import the libraries we need
import processing.video.*;
import ddf.minim.analysis.*;
import ddf.minim.*;

// initialize the video
Movie mov;

// where to start the video
float movieTime = 0;

// initialize audio input and amplitude analysis
Minim minim;  
AudioInput input;

// the range of input values to expect
float minimumIn = 0;
float maximumIn = 1;

// create the empty variable for our input
float inValue = 0.0;

void setup() {
  // set the size of the sketch
  size(1280, 720, P3D);
  
  // we have to load the video file
  mov = new Movie(this, "cheetahcut_mjpeg.mp4");

  // start with a white background
  background(255);
  
  minim = new Minim(this);
  // Create an Audio input and grab the 1st channel
  input = minim.getLineIn();

}

void draw() { 
  
  // audio is very hectic, so we want to smooth it
  float smoothing = 0.05;
  
  // here we read the amplitude of the playing audio
  inValue = inValue * smoothing + (1.0-smoothing) * input.left.level();
  
  // if there is a new frame in the video, read it
  if (mov.available()) {
    mov.read();
  }
  
  // the playback speed is calculated from our input
  float speed = max(0.0,map(inValue,minimumIn,maximumIn,0.0,0.6));
  movieTime += speed;
  // play the movie to the next frame and pause it
  mov.play();
  float time = movieTime%mov.duration(); // movieTime goes to infinity, but we want to stay inside the duration of the video
  mov.jump(time);
  mov.pause();

  // draw the video!
  image(mov, 0, 0, width, height);
}
