#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

uniform sampler2D texture;

uniform vec3 texOffset;
uniform float texAlpha;
varying vec4 vertColor;
varying vec4 vertTexCoord;

void main() {
  float texColorR = texture2D(texture, mod(vertTexCoord.st + vec2(texOffset.r,0.0),1.0)).r;
  float texColorG = texture2D(texture, mod(vertTexCoord.st + vec2(texOffset.g,0.0),1.0)).g;
  float texColorB = texture2D(texture, mod(vertTexCoord.st + vec2(texOffset.b,0.0),1.0)).b;
  vec4 texColor = vec4(texColorR, texColorG, texColorB, texAlpha);

  gl_FragColor = texColor;  
}
