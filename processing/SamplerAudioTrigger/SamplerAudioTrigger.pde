/**
 * Now, let's do some magic! How about triggering sound with sound?
 * This example shows how to make a simple sampler and sequencer with the Sound library.
 * Five different samples are loaded and played back at different pitches, in this
 * case 5 different octaves.
 * The samples are triggered when the microphone input moves above a threshold.
 * Each time a sound is played a colored rect with a random color is
 * displayed.
 * The rectangles are not pretty or very cool per se, but you should draw your own things anyway.
 * images for example.
 *
 * Dependencies (a.k.a. libraries you need to install):
 * - Sound
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound", don't search for "dependency")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 */

import processing.sound.*;

SoundFile[] file;

AudioIn input;
Amplitude loudness;
float previousVolume;

// Define the number of samples 
int numsounds = 5;

// Create an array of values which represent the octaves. 1.0 is playback at normal speed, 0.5 is half and 
// therefore one octave down. 2.0 is double so one octave up.
float[] octave = {0.25, 0.5, 1.0, 2.0, 4.0};

// The playSound array is defining how many samples will be played at each trigger event
int[] playSound = {1, 1, 1, 1, 1};

// The trigger is a float number that we can use as a threshold for our microphone to play the sounds
// Why a float, and not an integer as before you ask?
// Well, the microphone input will be between 0 and 1, so we need more precision.
// integers can only do full numbers, check this: https://processing.org/examples/integersfloats.html
float trigger;

// This array holds the pixel positions of the rectangles which are drawn each event
int[] posx = {0, 128, 256, 384, 512};


void setup() {
  size(640, 360);
  background(255);

  // Create an array of empty soundfiles
  file = new SoundFile[numsounds];

  // Load 5 soundfiles from a folder in a for loop. By naming the files 1., 2., 3., n.aif it is easy to iterate
  // through the folder and load all files in one line of code.
  for (int i = 0; i < numsounds; i++) {
    file[i] = new SoundFile(this, (i+1) + ".aif");
  }
  
  // Create an Audio input and grab the 1st channel
  input = new AudioIn(this, 0);

  // Begin capturing the audio input
  input.start();
  // start() activates audio capture so that you can use it as
  // the input to live sound analysis, but it does NOT cause the
  // captured audio to be played back to you. if you also want the
  // microphone input to be played back to you, call
  //    input.play();
  // instead (be careful with your speaker volume, you might produce
  // painful audio feedback. best to first try it out wearing headphones!)

  // Create a new Amplitude analyzer
  loudness = new Amplitude(this);

  // Patch the input to the volume analyzer
  loudness.input(input);

  trigger = 0.35;
  previousVolume = 0.0;
}

void draw() {
  fill(255,255,255,20);
  rect(0,0,width,height);
  strokeWeight(1);
  
  float volume = loudness.analyze();
  
  // If the volume is above the trigger, play the samples!
  if (volume > trigger) {
    // but only, if the previous volume was not above the trigger
    // otherwise the samples are continously triggered and we're in crazyland
    if (previousVolume <= trigger) {
      // Redraw the background every time to erase old rects
      background(255);

      // By iterating through the playSound array we check for 1 or 0, 1 plays a sound and draws a rect,
      // for 0 nothing happens.

      for (int i = 0; i < numsounds; i++) {      
        // Check which indexes are 1 and 0.
        if (playSound[i] == 1) {
          float rate;
          // Choose a random color and get set to noStroke()
          fill(int(random(255)), int(random(255)), int(random(255)));
          noStroke();
          // Draw the rect in the positions we defined earlier in posx
          rect(posx[i], 50, 128, 260);
          // Choose a random index of the octave array
          rate = octave[int(random(0, 5))];
          // Play the soundfile from the array with the respective rate and loop set to false
          file[i].play(rate, 1.0);
        }

        // Renew the indexes of playSound so that at the next event the order is different and randomized.
        playSound[i] = int(random(0, 2));
      }
      
      strokeWeight(10);
    }
  }
  // let's show the volume, so we know what we're doing
  stroke(0,255,255);
  line(volume * width, 0, volume * width, height);
  
  // let's show the threshold, so we know even better what we're doing
  stroke(255,125,0,255);
  line(trigger * width, 0, trigger * width, height);
  
  previousVolume = volume;
}
