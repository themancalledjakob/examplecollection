/**
 * Grab video and use optical flow for drawing
 * 
 * Dependencies (a.k.a. libraries you need to install):
 * - Video
 * - OpenCV for Processing
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 */


import gab.opencv.*;
import processing.video.*;

OpenCV opencv;
Movie video;

float startRecordingSec = 0.2;

void setup() {
  size(1280, 720, P2D);
  video = new Movie(this, "frogtiger_320x180.mp4");
  video.jump(0);
  video.play();
  println("here");
  opencv = new OpenCV(this, 320, 180);
  background(0);
}

void draw() {
  noStroke();
  blendMode(BLEND);
  tint(0,0,0,0);
  image(video,0,0,width,height);
  
  if (video.available()) {
    video.read();
    if (video.time() > startRecordingSec) {
    
    opencv.loadImage(video);
    //opencv.threshold(250);
    opencv.calculateOpticalFlow();
  
    int stepSize = 1;
  
    blendMode(SCREEN);
    for (int y = 0; y < opencv.flow.height(); y+=stepSize) {
      for (int x = 0; x < opencv.flow.width(); x+=stepSize) {
        PVector flowVec = opencv.getFlowAt(x, y);
        
        float mag = flowVec.mag();
        float intensity = 25;
        
        if (mag > 0.1 && mag < 10.0) {
          int rX = int(max(0,flowVec.x * intensity));
          int gX = int(max(0,flowVec.x * -intensity));
          int rY = int(max(0,flowVec.y * intensity));
          int gY = int(max(0,flowVec.y * -intensity));
          
          int r = rX + rY;
          int g = gY + gY;
          int b = int(mag * intensity * 0.5);
          
          float strokeW = mag * 2;
          
          stroke(r, g, b);
          strokeWeight(strokeW);
          
          float scaleX = width * 1.0/video.width;
          float scaleY = height * 1.0/video.height;
          
          int x1 = (int)min(x * scaleX,width-1);
          int y1 = (int)min(y * scaleY,height-1);
          
          int x2 = (int)min((x+flowVec.x) * scaleX,width-1);
          int y2 = (int)min((y+flowVec.y) * scaleY,height-1);
          line(x1,y1,x2,y2);
        }
      }
    }
    }
  }
}

void keyReleased() {
  // erase the drawing with space
  if (key == ' ') {
    blendMode(ADD);
    background(0);
  }
  
  if (key == 's') {
    saveFrame("screenshot-######.png");
  }
}

//void movieEvent(Movie m) {
//  m.read();
//}
