 Grab image from the camera input and draw movement flow

 # Dependencies (a.k.a. libraries you need to install):
 - OpenCV for Processing
 - Video
 
 # Install dependency:
 - click in menu bar on: Sketch/Import Library/Add Library
 - search for dependency (for example "Sound")
 - click install
 - restart processing to load examples in example browser (optional)

# Preview:
![Sample Video](preview.mp4)
