/**
 * Grab image from the camera input and draw movement flow
 * 
 * Dependencies (a.k.a. libraries you need to install):
 * - OpenCV for Processing
 * - Video
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 * Notes:
 * this example loads the first detected camera, you can do
 * printArray(Capture.list());
 * to list all available cameras in the console below.
 */

import gab.opencv.*;
import processing.video.*;

OpenCV opencv;
Capture video;

float maxX = 0;
float minX = 0;
float maxY = 0;
float minY = 0;

void setup() {
  size(1280, 720, P2D);
  video = new Capture(this, 320, 240);
  opencv = new OpenCV(this, 320, 240);
  video.start();
  background(0);
  blendMode(SCREEN);
}

void draw() {
  //fill(0,0,0,255);
  //rect(0,0,width,height);
  
  // draw camera normally in upper left corner
  tint(0,0,0,0);
  image(video,0,0);
  tint(255,255,255,255);
  
  opencv.loadImage(video);
  opencv.calculateOpticalFlow();

  // push everything to the right
  // check: https://processing.org/tutorials/transform2d/
  pushMatrix();
  translate(width/2,0);
  
  // set stroke color to red
  stroke(255,0,0);
  
  //blendMode(SCREEN);
  // draw the optical flow
  popMatrix();
  int stepSize = 2;

  for (int y = 0; y < opencv.flow.height(); y+=stepSize) {
    for (int x = 0; x < opencv.flow.width(); x+=stepSize) {
      PVector flowVec = opencv.getFlowAt(x, y);
      
      maxX = max(maxX,flowVec.x);
      minX = min(maxX,flowVec.x);
      maxY = max(maxY,flowVec.y);
      minY = min(maxY,flowVec.y);
      
      float mag = flowVec.mag();
      float intensity = 10;
      
      if (mag > 0.1 && mag < 10.0) {
        int rX = int(max(0,flowVec.x * intensity));
        int gX = int(max(0,flowVec.x * -intensity));
        int rY = int(max(0,flowVec.y * intensity));
        int gY = int(max(0,flowVec.y * -intensity));
        
        int r = min(255,rX + rY);
        int g = min(255,gY + gY);
        int b = min(255,int(mag * intensity * 0.5));
        int a = int((r+b+g));
        
        stroke(r, g, b);
        
        float scaleX = width * 1.0/video.width;
        float scaleY = height * 1.0/video.height;
        
        int x1 = (int)min(x * scaleX,width-1);
        int y1 = (int)min(y * scaleY,height-1);
        
        int x2 = (int)min((x+flowVec.x) * scaleX,width-1);
        int y2 = (int)min((y+flowVec.y) * scaleY,height-1);
        line(x1,y1,x2,y2);
      }
    }
  }
}

void keyReleased() {
  // erase the drawing with space
  if (key == ' ') {
    blendMode(ADD);
    background(0);
    blendMode(SCREEN);
  }
  
  if (key == 's') {
    saveFrame("screenshot-######.png");
  }
}

void captureEvent(Capture c) {
  c.read();
}
