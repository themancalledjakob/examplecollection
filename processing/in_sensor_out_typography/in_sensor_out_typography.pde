import geomerative.*;

import processing.serial.*;

// Declare the objects we are going to use, so that they are accesible from setup() and from draw()
RFont f;
RShape grp;
RPoint[] points;

int minimumIn = 44;
int maximumIn = 255;
float inValue = 0.0;

Serial arduino;

void setup(){
  // Initilaize the sketch
  size(720,1024);
  frameRate(60);

  // Choice of colors
  background(0);
  fill(255,102,0);
  stroke(0);
  
  // VERY IMPORTANT: Allways initialize the library in the setup
  RG.init(this);
  
  //  Load the font file we want to use (the file must be in the data folder in the sketch floder), with the size 60 and the alignment CENTER
  grp = RG.getText("Hello world!", "FreeSans.ttf", 72, CENTER);

  // Enable smoothing
  smooth();
  
  printArray(Serial.list());
  String portName = "";
  for (int i = 0; i < Serial.list().length; i++) {
    String element = Serial.list()[i];
    if (
      //element.indexOf("ttyUSB") > 0 // linux
      match(element,"(.*?)tty(.*?)USB") != null
      || match(element,"(.*?)tty(.*?)usb") != null 
      || element.indexOf("COM") > 0) { //windows
      portName = element;
      println("FOUND " + portName);
      break;
      
    }
  }
  arduino = new Serial(this, portName, 9600);
  arduino.clear();
}

void draw(){
  
  while ( arduino.available() > 0) {  // If data is available,
    inValue = max(minimumIn,arduino.read());         // read it and store it in 
  }
  println(inValue);
  // Clean frame
  //background(0);
  fill(0,70);
  rect(0,0,width,height);
  
  // pushMatrix saves rotation and scaling
  pushMatrix();
  // Set the origin to draw in the middle of the sketch
  translate(width/2, height/2);
  
  // Draw the group of shapes
  noFill();
  stroke(0,0,200,150);  
  
  RG.setPolygonizer(RG.UNIFORMLENGTH);
  RG.setPolygonizerLength(map(inValue, minimumIn, maximumIn, 3, 200));
  points = grp.getPoints();
  if (points != null) {
    for (int i = 0; i < grp.children.length; i++) {
      RPoint[] pts;
      pts = grp.children[i].getPoints();
      pushMatrix();
      fill(255,255,255);
      noStroke();
      beginShape();
      for(int p=0; p<pts.length; p++){
        vertex(pts[p].x, pts[p].y);
      }
      endShape();
      popMatrix();
    }
  }
  popMatrix();
  text(frameRate,20,20);
}
