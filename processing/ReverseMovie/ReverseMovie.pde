/**
 * Dynamic reverse playback example. This one works in almost all circumstances.
 *
 * The technique here is not to set the speed of the movie, but to calculate the frame
 * that should be shown and jump to it.
 * 
 * basically a movie is a collection of images (frames).
 * these are shown one after the other, superquick.
 * if we show frame 1, then frame 2, then frame 3 we go forwards.
 * if we show frame 3, then frame 2, then frame 1 we go backwards.
 * if we show frame 1, then frame 1 again, then frame 2, then frame 2 again, then... we go forwards, but slow
 * and so on.
 *
 * This method has a bit more code, but it is more stable than just setting mov.speed(value)
 *
 * Dependencies (a.k.a. libraries you need to install):
 * - Video
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for the name of the dependency (e.g. "Sound", or "OpenCV for Processing")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 */

import processing.video.*;

// the movie object, you need one for each movie
Movie mov;

// we have the speed as a global variable,
// so we can change it from anywhere, however we want
float speed = -1.0;

// the setup function, runs once at the beginning
// use it to set up things :-)
void setup() {
  // set the size (it looks nicer on my screen if I add P3D at the end)
  size(560, 406, P3D);
  
  // draw a black background
  background(0);
  
  // load the movie! Try different formats and see what works best if you have issues.
  // ideally format your movie as "Motion-Jpeg" or MJPEG with mp4 file ending.
  // but honestly, whatever works, works.
  // only try to reformat your movie if something is wrong
  mov = new Movie(this, "cheetahcut_mjpeg.mp4");
  
  // set the movie to loop
  mov.loop();
  
  // setting a fixed frameRate might make everything smoother
  frameRate(60);
}

// the draw function, runs over and over and over and over and over again
// we use it to draw the current frame of the movie
void draw() {
  // first, make sure that the movie is loaded before we use it
  // there are many ways to do this, but the simplest is
  // checking for the width! if it has a width, it is there.
  // if it doesn't have a width, it's not there
  if (mov.width > 0) {
    // now let's set the speed dynamically
    // we could make it react on the mouse?
    speed = map(mouseX,0,width,-2,2);
    
    // you can also animate the speed a bit with sinus
    // sinus is smoothly changing between -1 an 1, and therefore fantastic for animation
    // uncomment the following lines to see how it might work
    //float speedSinus = sin(millis() * 0.001);
    //speed = map(speedSinus,
    //            -1, // minimum input (sinus is minimum -1)
    //            1,  // maximum input (sinus is maximum 1)
    //            -1.8, // minimum output (-1.8 is the fastest reverse speed we want)
    //            -0.6);// maximum output (-0.6 is the slowest reverse speed we want)
    
    // calculating how long a single frame should be shown is simple.
    // we just divide 1.0 by the frameRate, since the frameRate is the amount of frames
    // that should be played per second, makes sense?
    // let's say we have four images that we want to show in one second.
    // then we show each image for 1/4 of a second, right?
    float frameTime = 1.0/mov.frameRate;
    
    // the next position we want to jump to is the current time, plus frameTime
    // and then of course, the frameTime multiplied with speed, so we can make
    // shorter or longer jumps depending on how fast we want to be
    float nextJump = mov.time() + (frameTime * speed);
    
    // now, we might jump out of the movie with the above code.
    // meaning we could get a negative value, which is impossible.
    // there is no negative time in a movie.
    // or we could get a value which is greater than the duration: also impossible.
    // the following code makes sure we stay within the duration
    // and smoothly go from beginning to end, and the other way around
    float safeJump = (nextJump + mov.duration())%mov.duration();
    
    // jump!
    mov.jump(safeJump);
  }
  
  // we can control opacity/transparency
  // so the movie is smoother or less smooth
  // 0 = fully transparent
  // 255 = fully opaque
  int opacity = 50;
  
  // set the opacity, everything drawn after this,
  // will be more or less transparent
  tint(255,255,255,opacity);
  
  // draw the movie across the whole screen!
  image(mov, 0, 0, width, height);
}

// finally, the function to read the movie whenever it has a new image to show
void movieEvent(Movie m) {
  m.read();
}
