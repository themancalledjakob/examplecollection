/**
 * Grab image from the camera input and draw movement flow
 * 
 * Dependencies (a.k.a. libraries you need to install):
 * - OpenCV for Processing
 * - Video
 *
 * Install dependency:
 * - click in menu bar on: Sketch/Import Library/Add Library
 * - search for dependency (for example "Sound")
 * - click install
 * - restart processing to load examples in example browser (optional)
 *
 * Notes:
 * this example loads the first detected camera, you can do
 * printArray(Capture.list());
 * to list all available cameras in the console below.
 */

import gab.opencv.*;
import processing.video.*;

OpenCV opencv;
Capture video;


void setup() {
  size(640, 480);
  printArray(Capture.list());
  video = new Capture(this, 320, 240);
  opencv = new OpenCV(this, 320, 240);
  video.start();
}

/*

___________________________________
|                |                |
|  normal camera |  quick drawing |
|                |  of flow       |
|                |                |
|________________|________________|
|                |                |
|                |   average      |
|                |   optical flow |
|________________|________________|

*/

void draw() {
  fill(0,0,0,255);
  rect(0,0,width,height);
  
  // draw camera normally in upper left corner
  image(video,0,0,width/2,height/2);
  
  opencv.loadImage(video);
  opencv.calculateOpticalFlow();

  // push everything to the right
  // check: https://processing.org/tutorials/transform2d/
  pushMatrix();
  translate(width/2,0);
  
  // set stroke color to red
  stroke(255,0,0);
  
  // draw the optical flow
  opencv.drawOpticalFlow();
  
  // undo push everything to the right
  popMatrix();
  
  // interesting is also what the average flow is.
  // that is a direction, so we have an x and a y value.
  // Processing likes to store directions as PVectors.
  // check: https://processing.org/reference/PVector.html
  //
  // we can get it like this:
  PVector averageFlow = opencv.getAverageFlow();
  
  // the flow can be very small, so we better scale it up
  // to make it visible
  int flowScale = 50;
  
  // set color and stroke thickness
  stroke(255);
  strokeWeight(2);
  
  // push everything to the bottom right corner
  pushMatrix();
  translate(width*0.75,height*0.775);
  
  // draw a line and a circle
  line(0, 0, averageFlow.x*flowScale, averageFlow.y*flowScale);
  circle(averageFlow.x*flowScale,averageFlow.y*flowScale,5);
  
  // undo push everything to the bottom right corner
  popMatrix();
}

// the next function is necessary to update the camera image
// don't forget this! otherwise your camera will just stay black
// and you don't see anything
void captureEvent(Capture c) {
  c.read();
}
