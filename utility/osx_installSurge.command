#!/bin/bash
currentDirectory="$(dirname "$BASH_SOURCE")"

read -p "this will install brew, a program to install other things (package manager). Are you ready? (ctrl + c to cancel, any key to continue)" var

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

brew update

# use brew to install npm, which is another package manager
brew install npm

# tell npm to put the packages in a directory you can access
mkdir ~/.npm-packages

npm config set prefix ~/.npm-packages

#finally install surge
npm install --global surge
